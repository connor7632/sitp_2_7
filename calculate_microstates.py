#!/usr/bin/env python3
"""Computer code to solve SITP Problem 2.7"""

import scipy.special  # for combinatorial function


def microstates(N, q):
    """A generator to return the microstates associated with
    q excitations distributed among N oscillators"""
    if N == 1:
        yield [q, ]
    else:
        yield from (microstate+[n, ]
                    for n in range(q+1)
                    for microstate in microstates(N-1, q-n))


if __name__ == "__main__":
    N = 4
    q = 2
    print("\n*** Output *** ")
    for i, microstate in enumerate(microstates(N, q)):
        print("%2d:   %s" % (i+1, "|".join([i*"*" for i in microstate])))
    print("\nEquation 2.9: ", scipy.special.comb(N+q-1, q, exact=True))
